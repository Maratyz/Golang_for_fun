# About the repo
- Golang for fun
- Open source the Golang codes whose procedure or algorithms are public

# License
All the programs within this repository available under GNU GENERAL PUBLIC LICENSE Version 3
