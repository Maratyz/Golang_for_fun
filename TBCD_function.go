import (
    "fmt"
    "encoding/binary"
)

func TBCD(tbcd []byte) int64 {
    identity := int64(0);
    for k := range tbcd {
            firstfourbits := int64(tbcd[k] >> 4)
            secondfourbits := (int64(tbcd[k]) & 8 | int64(tbcd[k]) & 4 | int64(tbcd[k]) & 2 | int64(tbcd[k]) & 1)
            if firstfourbits < 10 {
                identity = int64(100)*identity + firstfourbits + secondfourbits*int64(10)
            } else {
                identity = identity * 10 + secondfourbits
            }
    }
    return identity
}

func TBCD_IMEI(tbcd []byte) int64 {
    identity := int64(0);
    checkSum := int64(10);
    lastDigit := int64(10);
    for k := range tbcd {
            firstfourbits := int64(tbcd[k] >> 4)
            secondfourbits := (int64(tbcd[k]) & 8 | int64(tbcd[k]) & 4 | int64(tbcd[k]) & 2 | int64(tbcd[k]) & 1)
            if k < 7 {
                checkSum = checkSum + secondfourbits + (2*firstfourbits) % 10 + (2*firstfourbits/10) //https://imeidata.net/tool/calculator :  Luhn algorithm
            }
            if firstfourbits < 10 {
                //fmt.Printf("first digit: %d, second digit: %d, lun1: %d, lun2: %d, current byte: %d\n", firstfourbits, secondfourbits, (2*firstfourbits)%10,(2*firstfourbits/10), k)
                identity = int64(100)*identity + firstfourbits + secondfourbits*int64(10)
            } else {
                identity = identity * 10 + secondfourbits
            }
    }
    lastDigit = (10 - checkSum%10)%10 // %10 handling checkSum=10
    //fmt.Printf("checksum: %d, last digit of the IMEI: %d\n", checkSum, lastDigit)
    identity = int64(10)*(identity/100) + lastDigit
    return identity
}

func GUMMEI_convert(GUMMEI_bytes []byte) uint64 {
    GUMMEI := uint64(0);
    PLMN_IDENTITY := binary.BigEndian.Uint32([]byte{0,GUMMEI_bytes[0],GUMMEI_bytes[1],GUMMEI_bytes[2]}) //actual: 3bytes instead of 4 bytes
    MME_GROUPID := binary.BigEndian.Uint16(GUMMEI_bytes[3:5])
    MME_CODE := uint(GUMMEI_bytes[5])
    GUMMEI = uint64(100000000)*uint64(PLMN_IDENTITY)  + uint64(1000)*uint64(MME_GROUPID) + uint64(MME_CODE)
    return GUMMEI
}
